from django.db import models # noqa


class Course(models.Model):
    course_name = models.CharField(max_length=64, null=False)

    class LEVEL(models.Model):
        Trivial = 0, 'Trivial'
        Easy = 1, 'Easy'
        Medium = 2, 'Medium'
        Expert = 3, 'Expert'
        Hardcore = 4, 'Hardcore'
