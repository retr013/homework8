from classrooms.forms import ClassCreateForm, ClassUpdateForm, ClassroomFilter
from classrooms.models import Classroom

from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404  # noqa
from django.urls import reverse

# Create your views here.


def get_classroom(request):
    classroom = Classroom.objects.all().order_by('-id')

    filter = ClassroomFilter(  # noqa
        data=request.GET,
        queryset=classroom,
    )

    return render(
        request=request,
        template_name='classroom-list.html',
        context={'classroom': classroom}
    )


def create_classroom(request):
    if request.method == 'POST':

        form = ClassCreateForm(request.POST)

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('classroom:list'))

    else:

        form = ClassCreateForm()

    return render(
        request=request,
        template_name='classroom-create.html',
        context={'form': form}
    )


def update_classroom(request, id):  # noqa
    classroom = get_object_or_404(Classroom, id=id)

    if request.method == 'POST':

        form = ClassUpdateForm(
            data=request.POST,
            instance=classroom
        )

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('classroom:list'))

    else:

        form = ClassUpdateForm(instance=classroom)

    return render(request=request,
                  template_name='classroom-update.html',
                  context={
                        'form': form,
                        'classroom': classroom}
                  )

def delete_classroom(request, id):  # noqa
    classroom = get_object_or_404(Classroom, id=id)

    if request.method == 'POST':
        classroom.delete()
        return HttpResponseRedirect(reverse('classroom:list'))

    return render(
        request=request,
        template_name='classroom-delete.html',
        context={'classroom': classroom}
    )
