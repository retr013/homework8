from django.db import models

from groups.models import Group


class Classroom(models.Model):

    name = models.CharField(max_length=64, null=False)
    group = models.ManyToManyField(
        to=Group,
        related_name='group'
    )
