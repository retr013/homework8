"""lms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from classrooms.views import create_classroom, delete_classroom, get_classroom, update_classroom

from django.conf import settings # noqa
from django.urls import include, path # noqa

app_name = "classrooms"

urlpatterns = [
    path('', get_classroom, name='list'),
    path('create', create_classroom, name='create'),
    path('update/<int:id>', update_classroom, name='update'),
    path('delete/<int:id>', delete_classroom, name='delete'),
]
