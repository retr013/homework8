import random

from core.models import Person

from courses.models import Course

from django.db import models

from groups.models import Group


class Teacher(Person):
    class LEVEL(models.IntegerChoices):
        ASSISTANT = 0, "Assistant"
        ASSOCIATE = 1, "Associate"
        PROFESSOR = 2, "Professor"

    group = models.ForeignKey(
        to=Group,
        null=True,
        on_delete=models.SET_NULL,
        related_name='teachers'
    )

    course = models.ManyToManyField(
        to=Course,
        related_name='teachers_course'
    )

    level = models.PositiveSmallIntegerField(
        choices=LEVEL.choices,
        default=LEVEL.ASSOCIATE
    )

    @classmethod
    def _generate(cls):
        obj = super()._generate()
        obj.level = random.choice(cls.LEVEL.values)
        obj.save()

    def __str__(self):
        return f'{self.first_name} {self.last_name}, {self.age}'
