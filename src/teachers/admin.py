from django.contrib import admin # noqa

from teachers.models import Teacher


admin.site.register(Teacher)
