# Generated by Django 3.1.4 on 2021-01-31 16:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0002_auto_20210124_1334'),
        ('teachers', '0011_auto_20210131_1643'),
    ]

    operations = [
        migrations.AlterField(
            model_name='teacher',
            name='course',
            field=models.ManyToManyField(related_name='teachers_course', to='courses.Course'),
        ),
    ]
