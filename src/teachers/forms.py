import re # noqa

import django_filters
from django.core.exceptions import ValidationError # noqa
from django.forms import ModelForm

from email_validator import EmailNotValidError, validate_email

from teachers.models import Teacher


class TeacherFilter(django_filters.FilterSet):

    class Meta:

        model = Teacher
        fields = {
            'age': ['lt', 'gt'],
            'first_name': ['exact', 'icontains'],
            'last_name': ['exact', 'startswith'],
        }


class TeacherBaseForm(ModelForm):

    class Meta:
        model = Teacher
        # fields = ['first_name', 'last_name', 'age']
        fields = '__all__'

    def as_div(self):
        "Return this form rendered as HTML <p>s."
        return self._html_output(
            normal_row='<p%(html_class_attr)s>%(label)s %(field)s%(help_text)s</p>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )

    def clean_phone_number(self):
        SHORT_LENGTH = 13 # noqa

        phone_number = self.cleaned_data['phone_number']

        pattern = '(\(\d\d\d\)|\+\d\d\(\d\d\d\))\d\d\d\-\d\d\d\d' # noqa

        if not re.match(pattern, phone_number):
            raise ValidationError('Phone number is not correct')

        if len(phone_number) == SHORT_LENGTH:
            phone_number = '+38' + phone_number

        return phone_number

    def clean_age(self):
        employment_age = 50

        if self.cleaned_data['age'] > employment_age:
            raise ValidationError('Too old')

    def clean_email(self):

        BLACKLIST = ['asd.com'] # noqa

        email = self.cleaned_data['email']

        try:
            valid = validate_email(email)
            email = valid.email
            domain = valid.domain
            if domain in BLACKLIST:
                raise ValidationError('Your domain is prohibited')
        except EmailNotValidError:
            raise ValidationError('Email is invalid')

        teachers = Teacher.objects.filter(email=email).exclude(id=self.instance.id)

        if teachers:
            raise ValidationError('Email is not unique')

        return email


class TeacherCreateForm(TeacherBaseForm):
    pass


class TeacherUpdateForm(TeacherBaseForm):
    class Meta(TeacherBaseForm.Meta):
        exclude = ['age']
