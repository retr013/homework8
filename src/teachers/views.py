from copy import copy

from django.db.models import Q # noqa
from django.http import HttpResponse, HttpResponseRedirect # noqa
from django.shortcuts import render, get_object_or_404  # noqa
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from teachers.forms import TeacherBaseForm, TeacherCreateForm, TeacherFilter, TeacherUpdateForm
from teachers.models import Teacher


def get_teachers(request):
    teachers = Teacher.objects.all().select_related('group').order_by('-id')

    filter = TeacherFilter( # noqa
        data=request.GET,
        queryset=teachers,
    )

    # params = [
    #     'first_name',
    #     'first_name__startswith',
    #     'first_name__endswith',
    #     'last_name',
    #     'age',
    #     'age__gt'
    # ]
    #
    # for param_name in params:
    #     param_value = request.GET.get(param_name)
    #     if param_value:
    #         param_elems = param_value.split(',')
    #         if param_elems:
    #             or_filter = Q()
    #             for param_elem in param_elems:
    #                 or_filter |= Q(**{param_name: param_elem})
    #             teachers = teachers.filter(or_filter)
    #         else:
    #             teachers = teachers.filter(**{param_name: param_value})

    return render(request=request,
                  template_name='teachers-list.html',
                  context={'filter': filter})


def create_teacher(request):
    if request.method == 'POST':

        form = TeacherCreateForm(request.POST)

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('teachers:list'))

    else:

        form = TeacherCreateForm()

    return render(request=request,
                  template_name='teachers-create.html',
                  context={'form': form})


def update_teacher(request, id): # noqa
    teacher = Teacher.objects.get(id=id)

    if request.method == 'POST':

        form = TeacherUpdateForm(
            data=request.POST,
            instance=teacher
        )

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('teachers:list'))

    else:

        form = TeacherUpdateForm(instance=teacher)

    return render(request=request,
                  template_name='teachers-update.html',
                  context={'form': form})


def delete_teacher(request, id): # noqa
    teacher = get_object_or_404(Teacher, id=id)

    if request.method == 'POST':
        teacher.delete()
        return HttpResponseRedirect(reverse('teachers:list'))

    return render(
        request=request,
        template_name='teachers-delete.html',
        context={'teacher': teacher}
    )


class TeacherListView(ListView):
    model = Teacher
    template_name = 'teachers-list.html'
    success_url = reverse_lazy('students:list')
    paginate_by = 10
    context_object_name = 'teachers'

    def get_filter(self):
        return TeacherFilter(
            data=self.request.GET,
            queryset=self.model.objects.all().select_related('group')
        )

    def get_queryset(self):
        return self.get_filter().qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        context['query_params'] = self.request.GET.urlencode()

        if 'page' in self.request.GET:
            get_params = copy(self.request.GET)
            del get_params['page']
            context['query_params'] = get_params.urlencode()

        return context


class TeacherCreateView(CreateView):
    model = Teacher
    form_class = TeacherUpdateForm
    success_url = 'teachers:list'
    template_name = 'teachers-create.html'
    pk_url_kwarg = 'id'


class TeacherUpdateView(UpdateView):

    model = Teacher
    form_class = TeacherUpdateForm
    success_url = 'teachers:list'
    template_name = 'teachers-update.html'
    pk_url_kwarg = 'id'


class TeacherDeleteView(DeleteView):

    model = Teacher
    form_class = TeacherBaseForm
    success_url = 'teachers:list'
    template_name = 'teacher-delete.html'
    pk_url_kwarg = 'id'
