# from django.contrib.auth.mixins import LoginRequiredMixin
from copy import copy

from core.views import LoginRequiredMixin

from django.utils.http import urlencode # noqa

from django.db.models import Q # noqa
from django.http import HttpResponse, HttpResponseRedirect # noqa
from django.shortcuts import get_object_or_404, render # noqa
from django.urls import reverse, reverse_lazy # noqa
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from students.forms import StudentBaseForm, StudentCreateForm, StudentFilter, StudentUpdateForm
from students.models import Student


class StudentUpdateView(LoginRequiredMixin, UpdateView):

    model = Student
    form_class = StudentUpdateForm
    success_url = reverse_lazy('students:list')
    template_name = 'students-update.html'
    pk_url_kwarg = 'id'


class StudentListView(LoginRequiredMixin, ListView):
    model = Student
    template_name = 'students-list.html'
    success_url = reverse_lazy('students:list')
    paginate_by = 50
    context_object_name = 'students'

    def get_filter(self):
        return StudentFilter(
            data=self.request.GET,
            queryset=self.model.objects.all().select_related('group', 'headed_group')
        )

    def get_queryset(self):
        return self.get_filter().qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        # context['students'] = self.get_queryset()
        context['query_params'] = self.request.GET.urlencode()

        if 'page' in self.request.GET:
            get_params = copy(self.request.GET)
            del get_params['page']
            context['query_params'] = get_params.urlencode()

        return context


class StudentCreateView(LoginRequiredMixin, CreateView):
    model = Student
    form_class = StudentCreateForm
    success_url = reverse_lazy('students:list')
    template_name = 'students-create.html'


class StudentDeleteView(LoginRequiredMixin, DeleteView):
    model = Student
    form_class = StudentBaseForm
    success_url = reverse_lazy('students:list')
    template_name = 'students-delete.html'
    pk_url_kwarg = 'student_id'
