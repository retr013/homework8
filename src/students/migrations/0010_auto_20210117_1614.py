# Generated by Django 3.1.4 on 2021-01-17 16:14

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0009_auto_20210116_2032'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='graduate_date',
            field=models.DateField(default=datetime.date(2021, 1, 17)),
        ),
    ]
