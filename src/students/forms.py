import re

from django.core.exceptions import ValidationError
from django.forms import ModelForm

import django_filters


from email_validator import EmailNotValidError, validate_email

from students.models import Student


class StudentFilter(django_filters.FilterSet):

    class Meta:

        model = Student
        fields = {
            'age': ['lt', 'gt'],
            'first_name': ['exact', 'icontains'],
            'last_name': ['exact', 'startswith'],
        }


class StudentBaseForm(ModelForm):

    class Meta:
        model = Student
        # fields = ['first_name', 'last_name', 'age']
        fields = '__all__'

    def as_div(self):
        "Return this form rendered as HTML <p>s."
        return self._html_output(
            normal_row='<p%(html_class_attr)s>%(label)s %(field)s%(help_text)s</p>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )

    def clean_phone_number(self):
        SHORT_LENGTH = 13 # noqa

        phone_number = self.cleaned_data['phone_number']

        pattern = '(\(\d\d\d\)|\+\d\d\(\d\d\d\))\d\d\d\-\d\d\d\d' # noqa

        if not re.match(pattern, phone_number):
            raise ValidationError('Phone number is not correct')

        if len(phone_number) == SHORT_LENGTH:
            phone_number = '+38' + phone_number

        exist_students = Student.objects.filter(phone_number=phone_number).exclude(id=self.instance.id).exists()

        if exist_students:
            raise ValidationError('Email is not unique')

        return phone_number

    def clean(self):
        result = super().clean()

        enroll_date = self.cleaned_data['enroll_date']
        graduate_date = self.cleaned_data['graduate_date']

        if enroll_date > graduate_date:
            raise ValidationError('Enroll date coudn\'t be after graduate date')

        return result

    def clean_email(self):

        BLACKLIST = ['asd.com'] # noqa

        email = self.cleaned_data['email']

        try:
            valid = validate_email(email)
            email = valid.email
            domain = valid.domain
            if domain in BLACKLIST:
                raise ValidationError('Your domain is prohibited')
        except EmailNotValidError:
            raise ValidationError('Email is invalid')

        exist_students = Student.objects.filter(email=email).exclude(id=self.instance.id).exists()

        if exist_students:
            raise ValidationError('Email is not unique')

        return email


class StudentCreateForm(StudentBaseForm):
    pass


class StudentUpdateForm(StudentBaseForm):
    class Meta(StudentBaseForm.Meta):
        exclude = ['age']
