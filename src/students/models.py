import datetime
import random

from core.models import Person

import django
from django.db import models

from faker import Faker

from groups.models import Group


class Student(Person):

    enroll_date = models.DateField(default=datetime.date.today)
    graduate_date = models.DateField(default=django.utils.timezone.now)

    group = models.ForeignKey(
        to=Group,
        null=True,
        on_delete=models.SET_NULL,
        related_name='students'
    )

    @classmethod
    def _generate(cls):
        faker = Faker()
        obj = cls(
            first_name=faker.first_name(),
            last_name=faker.last_name(),
            age=random.randint(15, 105),
            group=random.choice(list(Group.objects.all())),
        )

        obj.save()
        return object
