# from django.contrib.auth.mixins import LoginRequiredMixin
from copy import copy

from core.views import LoginRequiredMixin

from django.db.models import Q # noqa
from django.http import HttpResponseRedirect, HttpResponse # noqa
from django.shortcuts import render, get_object_or_404  # noqa
from django.urls import reverse, reverse_lazy # noqa
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from groups.forms import GroupBaseForm, GroupCreateForm, GroupFilter, GroupUpdateForm
from groups.models import Group


class GroupUpdateView(LoginRequiredMixin, UpdateView):

    model = Group
    form_class = GroupUpdateForm
    # fields = ['name', 'headman']
    success_url = reverse_lazy('groups:list')
    template_name = 'groups-update.html'
    pk_url_kwarg = 'id'
    login_url = reverse_lazy('accounts:login')

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context['students'] = self.get_object().students.select_related('group', 'headed_group').all()
    #     return context


class GroupListView(LoginRequiredMixin, ListView):
    model = Group
    template_name = 'groups-list.html'
    success_url = reverse_lazy('students:list')
    login_url = reverse_lazy('accounts:login')
    context_object_name = 'groups'
    paginate_by = 5

    def get_filter(self):
        return GroupFilter(
            data=self.request.GET,
            queryset=self.model.objects.all().select_related('headman', 'teacher')
        )

    def get_queryset(self):
        return self.get_filter().qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        context['query_params'] = self.request.GET.urlencode()

        if 'page' in self.request.GET:
            get_params = copy(self.request.GET)
            del get_params['page']
            context['query_params'] = get_params.urlencode()
        return context


class GroupCreateView(LoginRequiredMixin, CreateView):
    model = Group
    form_class = GroupCreateForm
    template_name = 'groups-create.html'
    success_url = reverse_lazy('students:list')
    login_url = reverse_lazy('accounts:login')


class GroupDeleteView(LoginRequiredMixin, DeleteView):
    model = Group
    form_class = GroupBaseForm
    template_name = 'groups-delete.html'
    success_url = reverse_lazy('students:list')
    pk_url_kwarg = 'group_id'
    login_url = reverse_lazy('accounts:login')
