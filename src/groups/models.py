import random

from django.db import models


class Group(models.Model):

    name = models.CharField(max_length=64, null=False)
    project_name = models.CharField(max_length=84, null=False)
    students_amount = models.IntegerField(null=False, default=42)
    headman = models.OneToOneField(
        to='students.student',
        on_delete=models.SET_NULL,
        null=True,
        related_name='headed_group'

    )

    teacher = models.ForeignKey(
        to='teachers.teacher',
        null=True,
        on_delete=models.SET_NULL,
        related_name='teachers'
    )

    def __str__(self):
        return f'{self.name},' \
               f' {self.project_name},' \
               f' {self.students_amount},' \


    @classmethod
    def generate_groups(cls, count):

        proj_list = ['Games', 'LMS', 'Social engineering', 'Computer science', 'Java Script']

        for num in range(count):
            obj = cls(
                name='group_' + str(num),
                project_name=random.choice(proj_list),
                students_amount=random.randint(10, 30)
            )

            obj.save()
