import re # noqa

import django_filters
from django.core.exceptions import ValidationError # noqa
from django.forms import ChoiceField, ModelForm

from groups.models import Group

from students.models import Student


class GroupFilter(django_filters.FilterSet):

    class Meta:

        model = Group
        fields = ['name', 'project_name', 'students_amount']


class GroupBaseForm(ModelForm):

    class Meta:
        model = Group
        fields = '__all__'
        exclude = ['start_date']

    def as_div(self):
        "Return this form rendered as HTML <p>s."
        return self._html_output(
            normal_row='<p%(html_class_attr)s>%(label)s %(field)s%(help_text)s</p>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )

    def clean(self):
        result = super().clean()

        return result


class GroupCreateForm(GroupBaseForm):
    pass


class GroupUpdateForm(GroupBaseForm):

    class Meta(GroupBaseForm.Meta):
        exclude = ['start_date', 'headman']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        students = [
            (st.id, st.full_name())
            for st in self.instance.students.all()
        ]
        self.fields['headman_field'] = ChoiceField(
            choices=students,
            # initial=str(self.instance.headman.id),
            required=False
        )

    def save(self, commit=True):
        headman = Student.objects.get(id=int(self.cleaned_data['headman_field']))
        self.instance.headman = headman
        return super().save(commit)


# class GroupFilter(django_filters.FilterSet):
#     class Meta:
#         model = Group
#         fields = {
#             'name': ['icontains'],
#             'course': ['icontains'],
#         }
