from accounts.models import Profile

from django.contrib import admin # noqa


admin.site.register(Profile)
