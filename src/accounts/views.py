from accounts.forms import AccountProfileUpdateForm, AccountRegistrationForm, AccountUpdateForm

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render # noqa
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, UpdateView # noqa
from django.views.generic.edit import ProcessFormView


class AccountRegistrationView(SuccessMessageMixin, CreateView):
    model = User
    template_name = 'registration.html'
    success_url = reverse_lazy('accounts:login')
    form_class = AccountRegistrationForm
    success_message = 'You\'ve successfully registered'


class AccountLoginView(LoginView):
    success_url = reverse_lazy('index')
    template_name = 'login.html'

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('index')

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, f'User {self.request.user} has successfully logged in!')
        messages.warning(self.request, f'User {self.request.user} has successfully logged in!')
        messages.info(self.request, f'User {self.request.user} has successfully logged in!')
        return result


class AccountLogoutView(SuccessMessageMixin, LogoutView):
    template_name = 'logout.html'
    success_message = 'You\'ve logged out successfully'


# class AccountUpdateView(SuccessMessageMixin, UpdateView):
#     model = User
#     template_name = 'profile.html'
#     success_url = reverse_lazy('index')
#     form_class = AccountUpdateForm
#     success_message = 'Your profile is updated!'
#
#     def get_object(self, queryset=None):
#         return self.request.user


class ChangePasswordView(LoginRequiredMixin, PasswordChangeView):
    template_name = 'password-change.html'
    success_url = reverse_lazy('accounts:profile')


class AccountUpdateView(ProcessFormView):

    def get(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(instance=user)
        profile_form = AccountProfileUpdateForm(instance=profile)

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )

    def post(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(
            data=request.POST,
            instance=user
        )
        profile_form = AccountProfileUpdateForm(
            data=request.POST,
            files=request.FILES,
            instance=profile
        )

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            return HttpResponseRedirect(reverse('accounts:profile'))

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )
