import time

from core.models import Logger


class SimpleMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        start = time.time()

        response = self.get_response(request)

        elapsed = time.time() - start
        print(f'Request for url {request.path} took {elapsed} sec')

        return response


class PerfTrackerMiddleware:
    def __init__(self, get_response):

        self.get_response = get_response

    def __call__(self, request):

        start = time.time()

        response = self.get_response(request)

        elapsed = time.time() - start
        if request.user.is_authenticated:
            obj = Logger(
                user=request.user,
                path=request.path,
                execution_time=elapsed,
                query_params=request.GET.urlencode(),
                info='Executed successfully'
            )
            obj.save()

        return response
