from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect # noqa
from django.shortcuts import render, get_object_or_404 # noqa
from django.urls import reverse, reverse_lazy  # noqa


def index(request):
    return render(
        request=request,
        template_name='index.html'
    )


class LoginRequiredMixin(LoginRequiredMixin):
    login_url = reverse_lazy('accounts:login')
