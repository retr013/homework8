import re

from django.core.exceptions import ValidationError

from email_validator import EmailNotValidError, validate_email


def validate_email_for_prohibited_domain(value):

    BLACKLIST = ['asd.com'] # noqa

    try:
        valid = validate_email(value)
        domain = valid.domain
        if domain in BLACKLIST:
            raise ValidationError('Your domain is prohibited')
    except EmailNotValidError:
        raise ValidationError('Email is invalid')

    return value


def validate_phone_number(value):
    SHORT_LENGTH = 13 # noqa

    pattern = '(\(\d\d\d\)|\+\d\d\(\d\d\d\))\d\d\d\-\d\d\d\d' # noqa

    if not re.match(pattern, value):
        raise ValidationError('Phone number is not correct')

    if len(value) == SHORT_LENGTH:
        value = '+38' + value

    return value
