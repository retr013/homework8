import datetime
import random

from core.validators import validate_email_for_prohibited_domain, validate_phone_number

from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models # noqa

from faker import Faker


class Person(models.Model):

    class Meta:
        abstract = True

    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=84, null=False)
    age = models.IntegerField(null=False, default=42, validators=[MinValueValidator(10), MaxValueValidator(150)])

    email = models.EmailField(max_length=64, validators=[validate_email_for_prohibited_domain],
                              default='example@gmail.com')
    phone_number = models.CharField(null=False, max_length=20, validators=[validate_phone_number])
    birth_date = models.DateField(null=True)

    def __str__(self):
        return f'{self.first_name},' \
               f' {self.last_name},' \
               f' {self.email},' \
               f' {self.phone_number},' \
               f' {self.age}'

    @classmethod
    def _generate(cls):
        faker = Faker()
        obj = cls(
            first_name=faker.first_name(),
            last_name=faker.last_name(),
            age=random.randint(15, 105)
        )

        obj.save()
        return obj

    @classmethod
    def generate(cls, count):
        for _ in range(count):
            cls._generate()

    def full_name(self):
        return f'{self.first_name} {self.last_name}'

    def calculated_age(self):
        return datetime.datetime.now().year - self.birth_date.year


class Logger(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    path = models.CharField(max_length=128)
    create_date = models.DateTimeField(auto_now_add=True)
    execution_time = models.FloatField()
    query_params = models.CharField(max_length=64, null=True)
    info = models.CharField(max_length=128, null=True)
